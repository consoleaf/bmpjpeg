#include <iostream>
#include <fstream>
#include <regex>
#include <vector>
#include "lib/jpeg/jpeg.h"
#include "lib/bmp/bmp.h"

using namespace std;
using namespace marengo::jpeg;

regex bmp(".*.bmp$", regex_constants::icase);
regex jpeg(".*.jpe?g$", regex_constants::icase);

class Pixel {
public:
    unsigned char r, g, b;

    Pixel() = default;

    Pixel(int r, int g, int b) {
        this->r = r;
        this->g = g;
        this->b = b;
    }

    Pixel(Pixel *pPixel) {
        this->r = pPixel->r;
        this->g = pPixel->g;
        this->b = pPixel->b;
    }

    Pixel(vector<uint8_t> rgb) {
        this->r = rgb[0];
        this->g = rgb[1];
        this->b = rgb[2];
    }
};

class Img {
private:
    string initialFormat;
    string initialFilename;

    size_t width = 0, height = 0;
    vector<vector<Pixel>> pixels;

    static bool exists(const string &filename) {
        ifstream f(filename.c_str());
        bool res = f.good();
        f.close();
        return res;
    }

    void loadJPEG() {
        Image img(this->initialFilename);
        this->width = img.getWidth();
        this->height = img.getHeight();

        this->pixels.resize(this->height);

        for (size_t y = 0; y < this->height; ++y) {
            for (size_t x = 0; x < this->width; ++x) {
                this->pixels[y].emplace_back(img.getPixel(x, y));
            }
        }
    }

    void exportJPEG(const string &filename) {
        Image img;
        img.m_height = this->height;
        img.m_width = this->width;
        img.m_pixelSize = 3;
        img.m_bitmapData.clear();
        img.m_bitmapData.resize(this->height);
        for (int y = 0; y < this->height; ++y) {
            for (int x = 0; x < this->width; ++x) {
                img.m_bitmapData[y].push_back(this->pixels[y][x].r);
                img.m_bitmapData[y].push_back(this->pixels[y][x].g);
                img.m_bitmapData[y].push_back(this->pixels[y][x].b);
            }
        }
        img.savePpm(filename);
    }

    void loadBMP() {
        BmpImg img;
        img.read(this->initialFilename);

        this->width = (size_t) img.get_width();
        this->height = (size_t) img.get_height();

        this->pixels.resize(this->height);

        for (int y = 0; y < this->height; ++y) {
            for (int x = 0; x < this->width; ++x) {
                this->pixels[y].emplace_back(img.red_at(x, y), img.green_at(x, y), img.blue_at(x, y));
            }
        }
    }

    void exportBMP(const string &filename) {
        BmpImg img(this->width, this->height);
        for (int y = 0; y < this->height; ++y) {
            for (int x = 0; x < this->width; ++x) {
                Pixel tmp = pixels[y][x];
                img.set_pixel(x, y, tmp.r, tmp.g, tmp.b);
            }
        }
        img.write(filename);
    }

public:
    Img() = default;

    explicit Img(const string &filename) {
        this->read(filename);
    }

    void read(const string &filename) {
        if (regex_match(filename, bmp))
            this->initialFormat = "bmp";
        else if (regex_match(filename, jpeg))
            this->initialFormat = "jpeg";
        else
            throw invalid_argument("Incorrect filename.");
        if (exists(filename)) {
            this->initialFilename = filename;
        } else {
            throw invalid_argument("Specified file was not found.");
        }
        // Если мы попали сюда, значит файл существует и является jpeg/bmp.
        if (this->initialFormat == "jpeg")
            this->loadJPEG();
        else
            this->loadBMP();
    }

    void print_geometry() {
        cout << this->width << "x" << this->height << endl;
    }

    void exp(const string &new_filename) {
        string newFormat;
        if (regex_match(new_filename, bmp))
            newFormat = "bmp";
        else if (regex_match(new_filename, jpeg))
            newFormat = "jpeg";
        else
            throw invalid_argument("Incorrect filename.");
        if (exists(new_filename)) {
            throw invalid_argument("Specified file already exists.");
        }

        if (newFormat == "bmp")
            this->exportBMP(new_filename);
        else
            this->exportJPEG(new_filename);
    }
};

int main() {
    string fn = "img.jpeg";
    Img img;
    img.read(fn);
    img.exp("imgmgmggm.jpg");
    return 0;
}

